<?php
        session_start();
        if (!$_SESSION["username"]) {
                header('Location: http://3.209.127.91:8191/');
        }

        //if ($_SESSION["listings"] == "") {
        //        $_SESSION["listings"] = "selling";
        //}
        putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
        $conn = oci_connect("main", "main", "xe");
        if (!$conn) {
                print "Conn error";
        } else {
                //print "conn successful<br>";
        }
?>
<!doctype html>
<html lang="en">

  <head>
    <title>CarpoolND</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
              <div class="site-logo">
                <a href="#">CarpoolND</a>
              </div>
            </div>
	    <div class="col-9  text-right">


              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white active"><span class="icon-menu h3 text-white"></span></a></span>



              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
		  <li class="active"><a href="choose.php" class="nav-link">Home</a></li>
		  <li><a href="rides.php" class="nav-link">Available Rides</a></li>
		  <li><a href="../php_pages/test.php" class="nav-link">Schedule a Drive</a></li>
                  <li><a href="../php_pages/logout.php" class="nav-link">Logout</a></li>
                </ul>
              </nav>
            </div>
            <div class="col-9  text-right">


              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>



              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                </ul>
              </nav>
            </div>


          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay" style="background-image: url('images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center">
<div class="col-lg text-center">
              <h1>Hello <?php print $_SESSION["username"];?>!</h1>
              <p>Here are your CarpoolND Stats:</p>
	      <p>You've ridden in <?php 
		$query = 'select count(*) num from riders where ndid = '.$_SESSION["id"].'';
		$stmt = oci_parse($conn, $query);
		oci_define_by_name($stmt, 'NUM', $a);
                oci_execute($stmt);
		oci_fetch($stmt);
		print $a;
?> ride(s).</p>
	      <p>You've scheduled <?php
                $query = 'select count(*) num from rides where driver_ndid = '.$_SESSION["id"].' and complete_tag = 0';
                $stmt = oci_parse($conn, $query);
                oci_define_by_name($stmt, 'NUM', $a);
                oci_execute($stmt);
                oci_fetch($stmt);
                print $a;
?> trip(s).</p>
	      <p>You've driven <?php 
		$query = 'select count(*) num from rides where driver_ndid = '.$_SESSION["id"].' and complete_tag = 1';
                $stmt = oci_parse($conn, $query);
		oci_define_by_name($stmt, 'NUM', $a);
                oci_execute($stmt);
		oci_fetch($stmt);
                print $a;
?> trip(s).</p>
	      <p>You've had <?php 
		$query = 'select count(*) num from riders, rides where rides.ride_id = riders.ride_id and rides.driver_ndid = '.$_SESSION["id"].'';
                $stmt = oci_parse($conn, $query);
		oci_define_by_name($stmt, 'NUM', $a);
                oci_execute($stmt);
                oci_fetch($stmt);
                print $a;
?> passenger(s).</p>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>

  </body>

</html>
