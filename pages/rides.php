<?php
        session_start();
        if (!$_SESSION["username"]) {
                header('Location: http://3.209.127.91:8191/');
        }

        //if ($_SESSION["listings"] == "") {
        //        $_SESSION["listings"] = "selling";
        //}
        putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
        $conn = oci_connect("main", "main", "xe");
        if (!$conn) { 
                print "Conn error";
        } else {
                //print "conn successful<br>";
        }
?>
<!doctype html>
<html lang="en">

  <head>
    <title>CarpoolND</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
              <div class="site-logo">
                <a href="../pages/choose.php">CarpoolND</a>
              </div>
            </div>
<nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="../pages/choose.php" class="nav-link">Home</a></li>
                  <li class="active"><a href="#" class="nav-link">Available Rides</a></li>
                  <li><a href="../php_pages/test.php" class="nav-link">Schedule a Drive</a></li>
                  <li><a href="../php_pages/logout.php" class="nav-link">Logout</a></li>
                </ul>
              </nav>

            
          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay innerpage" style="background-image: url('images/hero_2.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 text-center">
              <h1>Find a Ride</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light">
      <div class="container">

<div class="row justify-content-center text-center">
        <div class="col-7 text-center mb-5">
          <h2>Available Rides</h2>
        </div>
      </div>
<div class="row">


<?php
	$query = 'select ride_id, first, last, phone, loc_start, loc_end, time_start, driver_ndid, full_tag, complete_tag, license, color, model, make, seats from rides, drivers, students where complete_tag = 0 and drivers.ndid != '.$_SESSION["id"].' and drivers.ndid = rides.driver_ndid and drivers.ndid = students.ndid order by hour_start';
	$stmt = oci_parse($conn, $query);
        $r = oci_execute($stmt);
	while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
		$time_arr = explode(":", $row["TIME_START"]);
                $hour = (int)$time_arr[0];
                if ($hour > 12 && $hour != 24){
                        $hour = $hour - 12;
                        $sign = 'PM';
                } else if ($hour == 24){
			$hour = 12;
			$sign = 'AM';
		} else {
                        $sign = 'AM';
                }
                $hour = (string)$hour;
                $row["TIME_START"] = $hour.':'.$time_arr[1].' '.$sign;
		print'<div class="col-lg-4 col-md-6 mb-4">';
            	print '<div class="item-1">';
                print '<div class="item-1-contents">';
                print '<div class="text-center">';
                print '<h3><b>' .$row["TIME_START"]. ' Leaving From '. $row["LOC_START"].'</b></h3>';
                print '</div>';
                print '<ul class="specs">';
                print  '<li>';
                print      '<span>Leaving From</span>';
                print      '<span class="spec">'.$row["LOC_START"].'</span>';
                print    '</li>';
                print    '<li>';
                print      '<span>Going To</span>';
                print      '<span class="spec">'.$row["LOC_END"].'</span>';
                print    '</li>';

                print    '<li>';
                print      '<span>Car</span>';
                print      '<span class="spec">'.$row["MAKE"].' '.$row["MODEL"].'</span>';
                print    '</li>';

                print    '<li>';
                print      '<span>License Plate</span>';
                print      '<span class="spec">'.$row["LICENSE"].'</span>';
                print    '</li>';

		print '<li>';
                print      '<span>Seats Left</span>';
		$seats = $row["SEATS"] - $row["FULL_TAG"];
                print      '<span class="spec">'.$seats.'</span>';
                print    '</li>';

                print '<li>';
                print      '<span>Riders</span>';
		print '<span class="spec">';
		$q = 'select * from students, riders where students.ndid = riders.ndid and riders.ride_id = '.$row["RIDE_ID"].'';
		$stmt2 = oci_parse($conn, $q);
        	$r2 = oci_execute($stmt2);
		while ($row2 = oci_fetch_array($stmt2, OCI_ASSOC+OCI_RETURN_NULLS)){
			print ''.$row2["USERNAME"].' ';
		}
                print      '</span>';
                print    '</li>';

                print    '<li>';
                print      '<span>Driver</span>';
                print      '<span class="spec">'.$row["FIRST"].' '. $row["LAST"].'</span>';
                print    '</li>';
                print    '<li>';
                print      '<span>Contact</span>';
                print      '<span class="spec">'.$row["PHONE"].'</span>';
                print  '</ul>';
                print  '<div style="text-align: center">';
		if ($row["SEATS"] != $row["FULL_TAG"]){
                print '<a href="../php_pages/ride.php?ride_id='.$row["RIDE_ID"].'&first='.$row["FIRST"].'&last='.$row["LAST"].'&phone='.$row["PHONE"].'" class="btn btn-primary">Request Ride</a>';
		}else {
		print '<h4 style="color:red;"> FULL </h4>';
		}
                print '</div>';
                print '</div>';
                print '</div>';
                print '</div>';
	}
?>

</div>
<div class="row justify-content-center text-center">
        <div class="col-7 text-center mb-5">
          <h2>My Rides</h2>
        </div>
      </div>
<div class="row">

<?php
        $query = 'select first, last, phone, loc_start, loc_end, ride_id, time_start, driver_ndid, full_tag, complete_tag, license, color, model, make, seats, hour_start from rides, drivers, students where complete_tag = 0 and drivers.ndid = '.$_SESSION["id"].' and drivers.ndid = rides.driver_ndid and drivers.ndid = students.ndid order by hour_start';
        $stmt = oci_parse($conn, $query);
        $r = oci_execute($stmt);
        while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
		$time_arr = explode(":", $row["TIME_START"]);
		$hour = (int)$time_arr[0];
		if ($hour > 12 && $hour != 24){
                        $hour = $hour - 12;
                        $sign = 'PM';
                } else if ($hour == 24){
                        $hour = 12;
                        $sign = 'AM';
                } else {
                        $sign = 'AM';
                }
		$hour = (string)$hour;
		$row["TIME_START"] = $hour.':'.$time_arr[1].' '.$sign;
                print'<div class="col-lg-4 col-md-6 mb-4">';
                print '<div class="item-1">';
                print '<div class="item-1-contents">';
                print '<div class="text-center">';
                print '<h3><b>' .$row["TIME_START"]. ' Leaving From '. $row["LOC_START"].'</b></h3>';
                print '</div>'; 
                print '<ul class="specs">';
                print  '<li>';
                print      '<span>Leaving From</span>';
                print      '<span class="spec">'.$row["LOC_START"].'</span>';
                print    '</li>';
                print    '<li>';
                print      '<span>Going To</span>';
                print      '<span class="spec">'.$row["LOC_END"].'</span>';
                print    '</li>';
                
                print    '<li>';
                print      '<span>Car</span>';
                print      '<span class="spec">'.$row["MAKE"].' '.$row["MODEL"].'</span>';
                print    '</li>';
                
                print    '<li>';
                print      '<span>License Plate</span>';
                print      '<span class="spec">'.$row["LICENSE"].'</span>';
                print    '</li>';
                
                print '<li>';
                print      '<span>Seats Left</span>';
		$seats = $row["SEATS"] - $row["FULL_TAG"];
                print      '<span class="spec">'.$seats.'</span>';
                print    '</li>';

                print '<li>';
                print      '<span>Riders</span>';
                print '<span class="spec">';
                $q = 'select * from students, riders where students.ndid = riders.ndid and riders.ride_id = '.$row["RIDE_ID"].'';
                $stmt2 = oci_parse($conn, $q);
                $r2 = oci_execute($stmt2);
                while ($row2 = oci_fetch_array($stmt2, OCI_ASSOC+OCI_RETURN_NULLS)){
                        print ''.$row2["USERNAME"].' ';
                }
                print      '</span>';
                print    '</li>';


                print    '<li>';
                print      '<span>Driver</span>';
                print      '<span class="spec">'.$row["FIRST"].' '. $row["LAST"].'</span>';
                print    '</li>';
                print    '<li>';
                print      '<span>Contact</span>';
                print      '<span class="spec">'.$row["PHONE"].'</span>';
                print  '</ul>';
                print  '<div style="text-align: center">';
                print '<a href="../php_pages/complete.php?ride_id='.$row["RIDE_ID"].'" class="btn btn-primary">Mark Complete</a>';
		print '  ';
                print '</div>';
                print '</div>';
                print '</div>';
                print '</div>';
        }       
?> 





</div>


	</div>
	</div>

    


    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>

  </body>

</html>

