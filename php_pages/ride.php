<?php
	session_start();
	if (!$_SESSION["username"]) {
		header('Location: http://3.209.127.91:8191/');
	}
	putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
	$conn = oci_connect("main", "main", "xe");
	if (!$conn) {
        	print "Conn error";
	} else {
		//make sure not already in ride, if already in it, don't do rest
		$query = 'select count(*) num from riders where ride_id = ' . $_GET["ride_id"].' and ndid = '.$_SESSION["id"].'';
		$stmt = oci_parse($conn, $query);
		oci_define_by_name($stmt, 'NUM', $r);
                oci_execute($stmt);
		oci_fetch($stmt);
		if ($r!=0){
			$_SESSION["requested_first"] = $_GET["first"];
                	$_SESSION["requested_last"] = $_GET["last"];
                	$_SESSION["requested_phone"] = $_GET["phone"];
			header('Location: http://3.209.127.91:8191/pages/repeatride.php');
			exit();
		}
        	$query = 'select * from rides, students, drivers where ride_id = ' . $_GET["ride_id"].' and students.ndid = rides.driver_ndid and drivers.ndid = students.ndid';
		$stmt = oci_parse($conn, $query);
		$r = oci_execute($stmt);
		$row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS);
		$new = $row["FULL_TAG"] + 1;
		$query = 'update rides set full_tag = '.$new.' where ride_id = '.$_GET["ride_id"].'';
		$stmt = oci_parse($conn, $query);
		$r = oci_execute($stmt);
		$_SESSION["requested_first"] = $row["FIRST"];
		$_SESSION["requested_last"] = $row["LAST"];
		$_SESSION["requested_phone"] = $row["PHONE"];
	
		$query = 'insert into riders (ndid, ride_id) values ('.$_SESSION["id"].', '.$_GET["ride_id"].')';
		$stmt = oci_parse($conn, $query);
                $r = oci_execute($stmt);
		if (!$r){
			print 'ERROR';
		}
		header('Location: http://3.209.127.91:8191/pages/reqride.php');
	}
?>
